<?php

namespace Drupal\tmgmt_memory\Controller;

use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\system\FileDownloadController;
use Drupal\tmgmt_memory\Tmx;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller to export usages.
 */
class ExportController implements ContainerInjectionInterface {

  /**
   * The EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file download controller.
   *
   * @var \Drupal\system\FileDownloadController
   */
  protected FileDownloadController $fileDownloadController;

  /**
   * The file system helpers.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The TMX processor.
   *
   * @var \Drupal\tmgmt_memory\Tmx
   */
  protected Tmx $tmxProcessor;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    /** @var FileSystemInterface $file_system */
    $file_system = $container->get('file_system');
    /** @var Tmx $tmx_processor */
    $tmx_processor = $container->get('tmgmt_memory.tmx');
    return new static(
      $entity_type_manager,
      $file_system,
      $tmx_processor,
      new FileDownloadController()
    );
  }

  /**
   * Constructs a ExportController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The source storage.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system helpers.
   * @param \Drupal\tmgmt_memory\Tmx $tmx_processor
   *   The TMX Processor.
   * @param \Drupal\system\FileDownloadController $file_download_controller
   *   The file download controller.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FileSystemInterface $file_system, Tmx $tmx_processor, FileDownloadController $file_download_controller) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->tmxProcessor = $tmx_processor;
    $this->fileDownloadController = $file_download_controller;
  }

  /**
   * Downloads a tarball of with the translations.
   */
  public function downloadExport() {
    $this->fileSystem->delete($this->fileSystem->getTempDirectory() . '/tmgmt_memory.tar.gz');

    $archiver = new ArchiveTar(\Drupal::service('file_system')->getTempDirectory() . '/tmgmt_memory.tar.gz', 'gz');

    $segment_translations = $this->entityTypeManager->getStorage('tmgmt_memory_segment_translation')->loadMultiple();
    $languages = array_unique(array_map(function (&$value){
      /** @var \Drupal\tmgmt_memory\SegmentTranslationInterface $value */
      return $value->getSource()->getLangcode();
    }, $segment_translations));
    foreach ($languages as $language) {
      $name = 'TMGMT_Memory' . '_' . $language . '.tmx';
      $archiver->addString($name, $this->tmxProcessor->export($language));
    }

    $request = new Request(['file' => 'tmgmt_memory.tar.gz']);
    return $this->fileDownloadController->download($request, 'temporary');
  }

}
